import java.util.Arrays;

public class Homework_Count_UP_Letters {

    public static void main(String[] args) {

       /* int index;
        String text = "Hello my flieeend";
        char[] inChar = text.toCharArray();
        String count = "";

        int[] countArray = new int[text.length()];

        for (int i = 0; i < inChar.length; i++) {
            index = count.indexOf(inChar[i]);
            if (index >= 0) {
                countArray[index]++;
            } else {
                count = count + inChar[i];
                countArray[count.length() - 1]++;
            }
        }

        for (int i = 0; i < count.length(); i++) {
            System.out.println(count.charAt(i) + " " + countArray[i]);
        }*/

        String myString = "Number oof symbolsss in the row.";
        String symbol;
        int count = 0;
        String temp;

        for (int j = 0; j < myString.length(); j++) {
            temp = myString;
            symbol = String.valueOf(myString.charAt(j));
            count = 0;
            for (int i = 0; i < myString.length(); i++) {
                if (!temp.contains(symbol)) {
                    break;
                }

                count++;
                int position = temp.indexOf(symbol);
                temp = temp.substring(position + 1);
            }
            myString = myString.replaceAll(symbol, " ");
            if (!symbol.equals(" ")) {
                System.out.println("Char " + symbol + " appears " + count + " times");
            }
        }
    }
}
