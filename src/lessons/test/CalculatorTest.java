import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import lessons.for_test.Calculator;
import org.junit.*;
//import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/*Cover tests Meeting*/
//@RunWith(Parameterized.class)
@RunWith(JUnitParamsRunner.class)
public class CalculatorTest {

    private Calculator calculator = new Calculator();

    @Parameterized.Parameter(value = 1)
    public int valueOne;

    @Parameterized.Parameter(value = 0)
    public int valueTwo;

    @Parameterized.Parameters
    public static Object[][] getData() {
        Object[][] data = {
                {1, 2},
                {-9, 7},
                {7, -6}
        };
        return data;
    }

    @Test
    public void parametersSum() {
        System.out.println(valueOne + " : " + valueTwo);
        Assert.assertEquals(valueOne + valueTwo, calculator.sum(valueOne, valueTwo));
    }


 //   @Rule
   // public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @BeforeClass
    public static void beforeAllTests() {
//        System.out.println("Before all tests");
    }

    @AfterClass
    public static void afterAllTests() {
//        System.out.println("After all tests");
    }

    @Before
    public void beforeEachTest() {
//        System.out.println("Before each test");
    }

    @After
    public void afterEachTest() {
//        System.out.println("After each test");
    }

    @Test
    public void shouldReturAddedValues() {
        Calculator calculator = new Calculator();

        Assert.assertEquals("Should return 10", 10, calculator.sum(8, 2));
    }

    @Test
    public void shouldReturAddedValues1() {

        Assert.assertEquals("Should return 10", 10, calculator.sum(8, 2));
    }

    @Test
    public void shouldReturnType() {
        System.out.println("Using rule");
//        systemOutRule.clearLog();
        calculator.showType();
 //       Assert.assertEquals("Programmer", systemOutRule.getLog());
    }

    @Test
    @Parameters({"5|4", "8|9"})
    public void usingJUnitParamLib(int value1, int value2) {
        Assert.assertEquals(value1 + value2, calculator.sum(value1, value2));
    }
}
