package use_mockito;

import lessons.for_test.Calculator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UseMockito {

    @Mock
    Calculator calculatorMock;

    @Test
    public void shouldMockObject() {
//        Calculator calculatorMock = Mockito.mock(Calculator.class);
        Calculator calculator = new Calculator();

        Mockito.when(calculatorMock.get()).thenReturn(88);
        Mockito.when(calculatorMock.getNumber()).thenCallRealMethod();

//        calculatorMock.getNumber();


//        Assert.assertEquals(88, calculatorMock.getNumber());
//        Mockito.verify(calculatorMock, Mockito.times(1)).getNumber();
//        Mockito.verify(calculatorMock, Mockito.atLeastOnce()).getNumber();
//        Mockito.verify(calculatorMock, Mockito.atMost(2)).getNumber();
//        Mockito.verify(calculatorMock, Mockito.times(2)).get();
        Mockito.verify(calculatorMock, Mockito.only()).getNumber();
        Mockito.verify(calculatorMock, Mockito.never()).sum(1, 2);

    }
}
