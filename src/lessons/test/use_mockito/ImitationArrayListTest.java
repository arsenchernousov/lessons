package use_mockito;

import homework.imitation_arraylist.ImitationArrayList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;

public class ImitationArrayListTest {

    private ImitationArrayList list = new ImitationArrayList();

    @Rule
    public final SystemOutRule systemOutRule = new SystemOutRule().enableLog();

    @Before
    public void beforeEachTest() {
        list.setArray(new int[5]);
    }

    @Test
    public void shoudAddElement() {
        int value = 78;
        list.addElement(value);
        int[] array = list.getArray();
        Assert.assertEquals(value, array[0]);
    }

    @Test
    public void shoudResize() {
        list.addElement(1);
        list.addElement(2);

        int[] array = list.getArray();

        Assert.assertEquals(2, array.length);

        int[] expecteds = {1, 2};
        Assert.assertArrayEquals(expecteds, array);
    }

    @Test
    public void shoudChangeElementByindex() {
        int index = 2;
        int element = 3;

        list.changeElementByIndex(index, element);

        int[] array = list.getArray();

        Assert.assertEquals(element, array[2]);
    }

    @Test
    public void shoudShowElementInOrder() {
        list.addElement(1);
        list.addElement(2);
        list.addElement(3);
        list.addElement(4);
        list.addElement(5);

        list.showElementInOrder();

        Assert.assertEquals("1 2 3 4 5 ", systemOutRule.getLog());

    }

    @Test
    public void shoudShowElementInReversOrder() {
        list.addElement(1);
        list.addElement(2);
        list.addElement(3);
        list.addElement(4);
        list.addElement(5);

        list.showElementInReversOrder();

        Assert.assertEquals("5 4 3 2 1 ", systemOutRule.getLog());
    }

    @Test
    public void shoudSortBubble() {
        list.addElement(2);
        list.addElement(4);
        list.addElement(6);
        list.addElement(8);
        list.addElement(10);

        list.sortBubble();

        Assert.assertEquals("10 8 6 4 2 ",systemOutRule.getLog());
    }


}





