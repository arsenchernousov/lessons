package use_mockito;

import homework.calculator.Calculator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class NewCalculatorTest {

    @Mock
    Calculator calculatorMock;

    @Test
    public void shouldSumOfValue() {
        Mockito.when(calculatorMock.getValueOne()).thenReturn(50);
        Mockito.when(calculatorMock.getValueTwo()).thenReturn(5);
        Mockito.when(calculatorMock.sumOfValue()).thenCallRealMethod();

        Assert.assertEquals(55, calculatorMock.sumOfValue());
    }

    @Test
    public void shouldSoubtaractionOfValue() {
        Mockito.when(calculatorMock.getValueOne()).thenReturn(40);
        Mockito.when(calculatorMock.getValueTwo()).thenReturn(10);
        Mockito.when(calculatorMock.subtractionOfValue()).thenCallRealMethod();

        Assert.assertEquals(30, calculatorMock.subtractionOfValue());
    }

    @Test
    public void shouldMultiplyOfValue() {
        Mockito.when(calculatorMock.getValueOne()).thenReturn(30);
        Mockito.when(calculatorMock.getValueTwo()).thenReturn(2);
        Mockito.when(calculatorMock.multiplyOfValue()).thenCallRealMethod();

        Assert.assertEquals(60, calculatorMock.multiplyOfValue());

    }

    @Test
    public void shouldDegreeOfValue() {
        Mockito.when(calculatorMock.getValueOne()).thenReturn(20);
        Mockito.when(calculatorMock.getValueTwo()).thenReturn(2);
        Mockito.when(calculatorMock.degreeOfValue()).thenCallRealMethod();

        Assert.assertEquals(10, calculatorMock.degreeOfValue());
    }

    @Test
    public void shouldRemainderOfTheDegree() {
        Mockito.when(calculatorMock.getValueOne()).thenReturn(23);
        Mockito.when(calculatorMock.getValueTwo()).thenReturn(2);
        Mockito.when(calculatorMock.remainderOfTheDegree()).thenCallRealMethod();

        Assert.assertEquals(1, calculatorMock.remainderOfTheDegree());
    }

}
