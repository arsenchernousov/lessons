package lessons.learn_class;

/**
 * Created by Admin on 17.05.2017.
 */
public class Person {
    private int age;//field with name age
    private int salary;//field with name salary

    private static long Id = 48;

    public Person(int age, int salary) {
        this.age = age;
        this.salary = salary;
    }

    public Person(int age) {
        this.age = age;
    }

    public Person() {
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {//(this, age)
        if (age < 0) {
            this.age = 0;
        } else {
            this.age = age;
        }
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
//        getId();
        this.salary = salary;
    }

    public static long getId() {
        return Id;
    }

    public static void setId(long id) {
        Id = id;
    }

    public void print() {
        System.out.println(age + salary);
    }
}
