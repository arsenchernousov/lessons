package lessons.learn_class;

/**
 * Created by Admin on 11.05.2017.
 */
public class Man {
    private int countOfPeople;//field - type name = value//0
    private short number;//field//0a
    private static int countOfRoom = 8;
    public final int COUNT = 5;

    {
//        System.out.println("Non-static block initialization");
    }

    static {
//        System.out.println("Static block initialization");
//        System.out.println("Hello world");
    }

    public Man(int newCountOfPeople, short newNumber) { // - constructor
//        System.out.println("Constructor");
        countOfPeople = newCountOfPeople;
        number = newNumber;
    }

    public Man(short newNumber, int newCountOfPeople) { // - constructor
//        System.out.println("Constructor");
        countOfPeople = newCountOfPeople;
        number = newNumber;
    }

    public Man(int countOfPeople) {
        this.countOfPeople = countOfPeople;
    }

    public Man(short number) {
        this.number = number;
    }



    public static int getCountOfRoom() {
//        getCountOfPeople();
//        countOfPeople = 45;

        return countOfRoom;
    }

    public static void setCountOfRoom(int countOfRoom) {
        Man.countOfRoom = countOfRoom;
    }

    public static void main(String[] args) {

    }

    /*public Man() {
    }*/

    public int getCountOfPeople() {
        countOfRoom = 78;
        return countOfPeople;
    }

    public void setCountOfPeople(int newCountOfPeople) {//(this, newCountOfPeople)
        if (newCountOfPeople < 0) {
            this.countOfPeople = 47;
        } else {
            this.countOfPeople = newCountOfPeople;
        }
    }

    public short getNumber() {
        return number;
    }

    public void setNumber(short newNumber) {
        countOfPeople = 45;
        number = newNumber;
    }

/*    public void print(int a) - signature of method */
/*    public - модификатор доступа
      void - возвращаемый тип
      print - название метода
      (int a) - входящие параметры
      */

    public void print(int a) {
        int r = 0;
        System.out.println(a);
    }

//    r = 45;
}
