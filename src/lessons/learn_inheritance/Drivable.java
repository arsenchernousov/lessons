package lessons.learn_inheritance;

import java.io.Closeable;

public interface Drivable extends Closeable, AutoCloseable {
    int COUNT = 5;
//    public abstract void drive();
     void drive();


    default void print() {
        System.out.println("Car");
    }
}
