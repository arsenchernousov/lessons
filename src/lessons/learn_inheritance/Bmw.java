package lessons.learn_inheritance;

public abstract class Bmw extends Car {
   private int capacityEngine;

    public Bmw(String color, String model, int capacityEngine) {
        super(color, model);
        this.capacityEngine = capacityEngine;
    }

    @Override
    public void getNumber() {

    }
}
