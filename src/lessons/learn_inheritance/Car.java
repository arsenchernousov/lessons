package lessons.learn_inheritance;

import java.io.Closeable;
import java.io.IOException;

public abstract class Car implements Drivable {
    private String color;//has-a
    private String model;

    public static int number;

    {
        System.out.println("Non-static block Car");//4
    }

    static {
        System.out.println("Static block Car");//5
    }

    public Car(String color, String model) {
        System.out.println("Constructor Car");//6
        this.color = color;
        this.model = model;
    }
//    546231
//    524613


    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public abstract void getNumber();

    @Override
    public void drive() {
        System.out.println("Drive");
    }


    @Override
    public void close() throws IOException {

    }


}

class TestInheritance {
    public static void main(String[] args) {
//        Mercedes mercedes = new Mercedes("Black", "X5", 5);
        Car mercedes = new Mercedes("Black", "X5", 5);
//        mercedes.drive();
//        Car  closeable = new Car();
    }

}

