package lessons.learn_inheritance;

import java.io.File;

public class Mercedes extends Car {//is-a


    private int classComfort;

    public static int number;

    {
        System.out.println("Non-static block Mercedes");//1
    }

    static {
        System.out.println("Static block Mercedes");//2
    }

    public Mercedes(String color, String model, int classComfort) {
        super(color, model);
        System.out.println("Constructor Mercedes");//3
        this.classComfort = classComfort;
    }

    public int getClassComfort() {
        return classComfort;
    }

    public void setClassComfort(final int classComfort) {
        this.classComfort = classComfort;
//        classComfort = 45;
    }

    @Override
    public void getNumber() {

    }
/*
    @Override
    public void drive() {
        System.out.println("Mercedes");
    }*/
}
