package lessons.learn_inheritance.polymorphysm;


public class Lion extends Animal {
    @Override
    public void sound() {
        System.out.println("R-r-r");
    }
}

class Cat extends Animal {
    @Override
    public void sound() {
        System.out.println("Myau - myau");
    }
}

class Mouse extends Animal {
    @Override
    public void sound() {
        System.out.println("Pi-pi");
    }
}

class TestPol {
    public static void main(String[] args) {
        Animal[] animals = new Animal[4];

        Animal animal = new Animal();
        Animal lion = new Lion();//Animal lion = new Animal();
        Animal cat = new Cat();
        Animal mouse = new Mouse();

        animals[0] = animal;
        animals[1] = lion;
        animals[2] = cat;
        animals[3] = mouse;

        Mouse mouse1 = new Mouse();
        mouse1.sound();
//        int a = "";


        for (Animal animal1 : animals) {
            animal1.sound();
        }

    }
}
