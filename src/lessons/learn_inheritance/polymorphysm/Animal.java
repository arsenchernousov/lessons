package lessons.learn_inheritance.polymorphysm;

public class Animal {
    protected void sound() {
        System.out.println("Say something");
    }
}

/*
* private  - only inside class
* package-private  - only inside package
* protected  - only inside package + childs
* public - all
* */