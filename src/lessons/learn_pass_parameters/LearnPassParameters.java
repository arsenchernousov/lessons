package lessons.learn_pass_parameters;

public class LearnPassParameters {

    public static void changeInt(int value) {// int value = 20;
        value = 15;
    }

    public static Integer changeInteger(Integer value) {
        value = 15;//new Integer(15)
        return value;
    }

    public static void changeGun(Gun gun) {
        gun.count = 15;
    }

    public int changeIntWirhReturn(int value) {// int value = 20;
        value = 15;
        return value;
    }

    public static void main(String[] args) {
        int v = 20;
        Integer i = 45;

        Gun gun = new Gun();

        gun.count = 50;

        changeGun(gun);

        System.out.println(gun.count);
//        v = changeIntWirhReturn(v);
        LearnPassParameters parameters = new LearnPassParameters();

//        v = parameters.changeIntWirhReturn(v);
         i = changeInteger(i);
//        changeIntWirhReturn(v);//int v1 = 20

//        System.out.println(i);
    }
}
