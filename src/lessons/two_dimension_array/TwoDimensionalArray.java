package lessons.two_dimension_array;


public class TwoDimensionalArray {
    public static void main(String[] args) {
        int[] array = new int[5];

        int[][] array2 = {
                {5, 8, 9, 4, 56},
                {1, 7, 3},
                {3, 7, 5, 4}
        };

        int[][] array3 = new int[5][6];//0


        /*for (int i = 0; i < array2.length; i++) {
            for (int j = 0; j < array2[i].length; j++) {
                System.out.println(array2[i][j]);
            }
            System.out.println();
        }*/

        int sumFirstLine = 0;
        int sumLastLine = 0;

        int sumFirstRow = 0;
        int sumLastRow = 0;

        for (int i = 0; i < array2.length; i++) {
            /*sumFirstLine += array2[0][i];
            sumLastLine += array2[2][i];*/
            sumFirstRow += array2[i][0];
            sumLastRow += array2[i][2];
        }

//        System.out.println(sumFirstLine + "\n" + sumLastLine);
//        System.out.println(sumFirstRow + "\n" + sumLastRow);

//        В двумерном массиве целых чисел определить, сколько раз в нем встречается элемент со значением 5.

        int count = 0;

        for (int i = 0; i < array2.length; i++) {
            for (int j = 0; j < array2[i].length; ++j) {
                if (array2[i][j] == 9) {
                    ++count;
                }
            }
        }
        System.out.println(count);

    }
}
