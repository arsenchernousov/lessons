package lessons.condition;

/**
 * Created by Admin on 03.04.2017.
 */
public class LearnCondition {
    public static void main(String[] args) {
        int a = 15;

        /*if (a < 10) {
            System.out.println("a < 10");
        }

        if (a > 10) {
            System.out.println("a > 10");
        }

//        замена вышеперечисленных if
        if (a < 10) {
            System.out.println("a < 10");
        } else {
            System.out.println("a > 10");
        }

        if (a > 10) {
            System.out.println("a > 10");
        } else if (a == 10) {
            System.out.println("a == 10");
        } else {
            System.out.println("a < 10");
        }*/

//        ternary operator
//        condition ? (statement if condition is true) : (statement if condition is false)

        /*int r = (1 == 3) ? 15 : 26;
//        (1 == 3) ? System.out.println("a < 10") : 26;
        int y = (a < 10) ?
                ((a < 5) ? 16 : 11)
                : 25;

        if (a < 10) {
            if (a < 5) {
                y = 16;
            } else {
                y = 11;
            }
        } else {
            y = 25;
        }

        System.out.println("r = " + r);*/
        int i = 0;

        int t = i++ + ++i + ++i + i++;//0 + 2 + 3 + 3

        System.out.println("t = " + t);
    }
}
