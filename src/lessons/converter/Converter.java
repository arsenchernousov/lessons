package lessons.converter;


public class Converter {
    public static void main(String[] args) {
        System.out.println(Integer.toBinaryString(37));
        System.out.println(Integer.toOctalString(37));
        System.out.println(Integer.toHexString(3239));
    }
}
