package lessons.learn_wrapper;

/**
 * Created by Admin on 20.05.2017.
 */
public class Wrapper {

    public static void changeValue(String value) {
        String hello = value.concat("Hello");
    }

    public static void changeValue(Integer value) {
        value = 89;//new Integer(89)
    }

    //    public static void passParameters(int a, int b, int c) {
//    public static void passParameters(int[] a) {
    public static void passParameters(int count, int... a) {
        System.out.println(a[0]);
    }

    public static void main(String[] args) {
        int a = 15;

        Integer s1 = 8;

        int[] array = {1, 5, 6, 2};

        passParameters(1, 2, 3, 5, 8, 9, 11);
        String str = "Run";
//        changeValue(str);
//        changeValue(s1);

//        System.out.println(str);
//        System.out.println(s1);

        Integer s = new Integer(8);

        s1 = a;//new Integer(15)

//        a = s;//new Integer(8) -> int d = 8
    }
}
