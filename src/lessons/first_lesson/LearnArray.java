package lessons.first_lesson;

public class LearnArray {
    public static void main(String[] args) {
        int[] array = new int[50];
        /*array[0] = 89;
        array[1] = 9;
        array[2] = 8;
        array[3] = 56;
        array[4] = 47;*/

        int a = 10;
        int b = 15;

       /* a++;//postfix form
        ++a;//prefix form*/
        /*System.out.println(a++);
        System.out.println(a);*/

        /*System.out.println(++a);
        System.out.println(a);*/

        /*int s = ++a +
                a++ +
                ++a;
                --a;
                a--;
        System.out.println(s);
        System.out.println(a);*/

       /* a = a + 1;
        a /= 1;

        System.out.println(5 / 2.0);
        System.out.println(5 % 2);*/

        for (int i = 0; i < array.length; ++i) {
            array[i] = i + 9;
        }

        ///iter0 -> i == 0; array[0] = 0;
        ///iter1 -> i == 1; array[1] = 2;
        ///iter2 -> i == 2; array[2] = 4;
        ///iter3 -> i == 3; array[3] = 6;
        ///iter4 -> i == 4; array[4] = 8;

        for (int i = 0; i < array.length; i++) {
//            System.out.println(array[i]);
        }

        int i = 0;

        while (i < array.length) {
            array[i] = i * 2;
            ++i;
        }

        i = 0;

        do {
            array[i] = i * 2;
            ++i;
        } while (i < array.length);

    }

}
