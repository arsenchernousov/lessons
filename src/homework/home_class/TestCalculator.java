package homework.home_class;


public class TestCalculator {
    public static void main(String[] args) {

        Calculator calculator = new Calculator(3, 2);


        int resultPlus = calculator.plus();

        calculator.setValueOne(18);
        calculator.setValueTwo(7);

        int resultMinus = calculator.minus();

        calculator.setValueOne(14);
        calculator.setValueTwo(6);

        int resultMultiplication = calculator.multiplication();
        int resultSegmentation = calculator.segmentation();
        int resultrRemainOfSegmentation = calculator.remainOfSegmentation();

        System.out.println(resultPlus);
        System.out.println(resultMinus);
        System.out.println(resultMultiplication);
        System.out.println(resultSegmentation);
        System.out.println(resultrRemainOfSegmentation);
    }

}
