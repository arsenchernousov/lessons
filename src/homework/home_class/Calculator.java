package homework.home_class;
/*
* Создать класс Калькулятор с двумя полями для выполнения мат.операций(+, -, *, /, %):
 - создать конструктор
 - для каждой операции создать отдельный метод без параметров
 - один объект
 - для каждой операции(при вызове методу) менять параметры через сеттеры
* */

public class Calculator {
    private int valuOne;//0
    private int valueTwo;

    public Calculator(int valuOne, int valueTwo) {
        this.valuOne = valuOne;
        this.valueTwo = valueTwo;
    }

    public void setValueOne(int newValueOne) {
        valuOne = newValueOne;
    }

    public int getValuOne() {
        return valuOne;
    }

    public void setValueTwo(int newValueTw0) {
        valueTwo = newValueTw0;
    }

    public int getValuTwo() {
        return valueTwo;
    }

    public int plus() {
        return valuOne + valueTwo;
    }

    public int minus(){return valuOne - valueTwo; }

    public int multiplication (){return valuOne * valueTwo; }

    public int segmentation (){return valuOne / valueTwo; }

    public int remainOfSegmentation (){return valuOne % valueTwo; }




}
