package homework.homework_lesson3;

/**
 * Created by Admin on 28.04.2017.
 */
public class Homework_array2 {
    public static void main(String[] args) {

        int[][] array = new int[6][6];

        for (int i = 0; i < array.length; i++) {
            int sumEachLine = 0;
            for (int f = 0; f < array[i].length; f++) {
                array[i][f] = i + f * 2;
                System.out.print(array[i][f] + "\t");
//                sumEachLine += 1;
                sumEachLine = sumEachLine + array[i][f];
            }
            System.out.println("Sum of " + i + " line = " + sumEachLine);
//            sumEachLine = 0;
            System.out.print("\n");
        }

        for (int i = 0; i < array.length; i++) {
            int sumEachRow = 0;
            for (int f = 0; f < array[i].length; f++) {
                sumEachRow = sumEachRow + array[f][i];
            }
            System.out.println("Sum of row " + i + " = " + sumEachRow);
        }
    }
}

// i == 0; f == 0; array[0][0] == 0; sumEachLine = 0 + 0(0);
// i == 0; f == 1; array[0][1] == 2; sumEachLine = 0 + 2(2);
// i == 0; f == 2; array[0][2] == 4; sumEachLine = 2 + 4(6);
