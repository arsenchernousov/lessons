package homework.calculator;


import java.util.Random;

public class Calculator {

    public int sumOfValue() {
        return getValueOne() + getValueTwo();
    }

    public int subtractionOfValue() {
        return getValueOne() - getValueTwo();
    }

    public int multiplyOfValue() {
        return getValueOne() * getValueTwo();
    }

    public int degreeOfValue() {
        return getValueOne() / getValueTwo();
    }

    public int remainderOfTheDegree() {
        return getValueOne() % getValueTwo();
    }

    public int getValueOne() {
        return new Random().nextInt(20);
    }

    public int getValueTwo() {
        return getValueOne();
    }

}
