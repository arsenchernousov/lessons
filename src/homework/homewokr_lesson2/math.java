package homework.homewokr_lesson2;

public class math {
    // 1015, 25789, 638
    public static void main(String[] args) {
        // 1015 = 111110111(binary)
        // 1015 = 1767(
        // 1015 = 3F7

        // 25789 = 110010010111101
        // 25789 = 62275
        // 25789 = 64bd

        // 638 = 1001111110
        // 638 = 1176
        // 638 = 27E

        System.out.println(Integer.toBinaryString(1015));
        System.out.println(Integer.toBinaryString(25789));
        System.out.println(Integer.toBinaryString(638));

        System.out.println(Integer.toHexString(1015));
        System.out.println(Integer.toHexString(25789));
        System.out.println(Integer.toHexString(638));

        System.out.println(Integer.toOctalString(1015));
        System.out.println(Integer.toOctalString(25789));
        System.out.println(Integer.toOctalString(638));

        int a = 4;
        int b = 4;
        int c = a + b;
        int d = a - b;
        int e = a * b;
        int f = a / b;
        System.out.println("Result = " + c);
        System.out.println("Result = " + d);
        System.out.println("Result = " + e);
        System.out.println("Result = " + f);

        int i = 0;
//        int u = i++ + ++i + ++i + ++i + i++;
        int r = i++ + ++i + ++i + ++i + i++ + ++i + ++i + ++i;//0+2 + 3 + 4 + 4 + 6 + 7 + 8
        System.out.println(r);

    }


}


