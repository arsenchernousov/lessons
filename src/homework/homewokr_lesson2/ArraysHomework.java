package homework.homewokr_lesson2;

public class ArraysHomework {
    public static void main(String[] args) {

        int[] array = new int[20];
        int[] array2 = new int[20];

        for (int i = 0; i < array.length; i++) {
            array[i] = i * 2;
            System.out.print("\t" + array[i]);
        }

        System.out.println();

        for (int i = 0; i < array.length; i++) {
            if (i % 4 == 0 && array[i] > 3 && array[i] < 16) {
                array2[i] = array[i];
            }
            System.out.print("\t" + array2[i]);
        }

    }
}
