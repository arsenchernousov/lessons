package homework.Airport;

public class Flight {

    private int timeInFlight;
    private CountryOfArrival arrival;
    private CountryOfDeparture departure;
    private int numberOfSeats;


    public Flight(int timeInFlight, CountryOfArrival arrival, CountryOfDeparture departure, int numberOfSeats) {
        this.timeInFlight = timeInFlight;
        this.arrival = arrival;
        this.departure = departure;
        this.numberOfSeats = numberOfSeats;
    }

    public void setTimeInFlight(int timeInFlight) {
        this.timeInFlight = timeInFlight;
    }

    public int getTimeInFlight() {
        return timeInFlight;
    }


    public void setArrival(CountryOfArrival arrival) {
        this.arrival = arrival;
    }

    public CountryOfArrival getArrival() {
        return arrival;
    }


    public void setDeparture(int CountryOfDeparture) { this.departure = departure;}

    public CountryOfDeparture getDeparture() {
        return departure;
    }


    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }


    public void printFieldsFlight() {
        System.out.println("Time in Flight =" + timeInFlight +
                " Country of Arrival" + arrival +
                " Country of Departure" + departure +
                " Number of Seats" + numberOfSeats);
    }


}
