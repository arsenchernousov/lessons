package homework.Airport;

public class FlyingService {
    private Flight[] airFlight;

    public FlyingService() {
        airFlight = new Flight[15];
    }

    public void addFlight(Flight flight) {

        for (int i = 0; i < airFlight.length; i++) {
            if (airFlight[i] == null) {
                airFlight[i] = flight;
                break;
            }

        }

        //showSuitableFlight(airFlight.getArrival(), airFlight.getDeparture() );
    }

    public void showSuitableFlight(CountryOfArrival arrival, CountryOfDeparture departure, int numberOfSeats) {
        for (Flight element : airFlight) {
            if (element != null && element.getArrival() == arrival
                    && element.getDeparture() == departure
                    && element.getNumberOfSeats() == numberOfSeats) {


                element.printFieldsFlight();
            }
        }
    }

    private void resizeArray() {
        boolean shouldResize = false;
        for (Flight element : airFlight) {
            if (element == null) {
                shouldResize = true;
                break;
            }
        }

        if (airFlight[airFlight.length - 1] != null) {
        }

        if (!shouldResize) {
            Flight[] temp = new Flight[airFlight.length * 2];
            for (int i = 0; i < airFlight.length; i++) {
                temp[i] = airFlight[i];

            }
            airFlight = temp;
        }
    }

    public void searchByTimeInFlightAndNumberOfSeats(int timeInFlight, int numberOfSeats) {
        for (Flight flight : airFlight) {
            if (flight != null &&
                    flight.getTimeInFlight() == timeInFlight &&
                    flight.getNumberOfSeats() == numberOfSeats) {
                flight.printFieldsFlight();
            }

        }
    }
}




