package homework.acquaintance;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class MeetingService {

    public final static Path PATH = Paths.get("./logs.txt");

    private Man[] arrayMan;//null

    public MeetingService() {
        arrayMan = new Man[10];
    }

    public void addMan(Man man) throws IOException {
        if (man.getAge() < 18) {
            System.out.println("Your age is smaller than need");
            return;
        }

        resize();

        String log = LocalDateTime.now() + " addMan " + man;
        writeLog(log);

        for (int i = 0; i < arrayMan.length; i++) {
            if (arrayMan[i] == null) {
                arrayMan[i] = man;
                break;
            }
        }
        searchByGenderAndAge(man.getGender(), man.getAge());
    }


    private void searchByGenderAndAge(Gender gender, int age) throws IOException {
        for (Man element : arrayMan) {
            if (element != null && element.getGender() == gender && element.getAge() == age) {
                System.out.println(element);
            }
        }

        String log = LocalDateTime.now() + " searchByGenderAndAge " + gender + age;
        writeLog(log);
    }

    private void resize() {
        boolean shouldResize = false;
        for (Man element : arrayMan) {
            if (element == null) {
                shouldResize = true;
                break;
            }

        }

        if (!shouldResize) {
            Man[] temp = new Man[arrayMan.length * 2];
            for (int i = 0; i < arrayMan.length; i++) {
                temp[i] = arrayMan[i];
            }
            arrayMan = temp;
        }
    }


    public void searchByFirstAndSecondName(String firstName, String secondName) throws IOException {

        for (Man man : arrayMan) {
            if (man != null &&
                    man.getFirstName().equals(firstName) &&
                    man.getSecondName().equals(secondName)) {
                System.out.println(man);
            }
        }
    }

    public void showAllPersons() {
        for (Man man : arrayMan) {
            if (man != null) {
                System.out.println(man);
            }
        }
    }

    private void writeLog(String log) throws IOException {
        Files.write(PATH, ("\n".concat(log)).getBytes(), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
    }
}

