package homework.acquaintance;

import java.util.Scanner;

public class Menu {
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        MeetingService service = new MeetingService();
        while (true) {
            menu();
            int key = scanner.nextInt();
            try {
                switch (key) {
                    case 1: {
                        int ageOfUser = Integer.parseInt(getInfoFromUser("Please enter your age"));

                        String firstName = getInfoFromUser("Please enter your First Name");

                        String secondName = getInfoFromUser("Please enter your Second Name");

                        String male = getInfoFromUser("Please enter your gender");

                        Gender gender = Gender.valueOf(male.toUpperCase());

                        int numberOfChildren = Integer.parseInt(getInfoFromUser("Please enter your number of children"));

                        String cityFromUser = getInfoFromUser("Please enter your city");
                        City city = City.valueOf(cityFromUser.toUpperCase());

                        Man man = new Man(ageOfUser, gender, firstName, secondName, numberOfChildren, city);

//                        service.createLogForAddMan();
                        service.addMan(man);

                        break;

                    }
                    case 2: {
                        String firstName = getInfoFromUser("Enter your first name");
                        String secondName = getInfoFromUser("Enter your second name");

                        service.searchByFirstAndSecondName(firstName, secondName);
//                        service.createLogForSearchByFirstAndSecondName();


                        break;
                    }

                    case 3: {



                        break;
                    }
                    case 4: {
                        service.showAllPersons();
                    }
                    case 5: {

                        String gender1 = getInfoFromUser("Enter gender for search");
                        Gender gender = Gender.valueOf(gender1.toUpperCase());

                        int age = Integer.parseInt(getInfoFromUser("Enter age for search"));

                        String male = getInfoFromUser("Please enter your gender");

                        int numberOfChildren = Integer.parseInt(getInfoFromUser("Please enter your number of children"));

                        String cityFromUser = getInfoFromUser("Please enter your city");
                        City city = City.valueOf(cityFromUser.toUpperCase());


                    }
                    return;
                    default:
                        return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    private static String getInfoFromUser(String message) {
        System.out.println(message);
        return scanner.next();
    }

    public static void showLog(){

    }

    public static void menu() {
        System.out.println("1) To register \n" +
                "2) Search by First name and Second name\n" +
                "3) Search by Gender and Age\n" +
                "4) Show all people\n" +
                "5) Smart search\n" +
                "6) Show log\n");


    }


}
