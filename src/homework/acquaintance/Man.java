package homework.acquaintance;

public class Man {

    private int age;
    private Gender gender;
    private String firstName;
    private String secondName;
    private int countOfChilden;
    private City city;
    

    public Man(int age, Gender gender, String firstName, String secondName, int countOfChilden, City city) {
        this.age = age;
        this.gender = gender;
        this.firstName = firstName;
        this.secondName = secondName;
        this.countOfChilden = countOfChilden;
        this.city = city;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Gender getGender() {
        return gender;

    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }


    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }


    public String getSecondName() {
        return secondName;
    }


    public void setCountOfChilden(int countOfChilden) {
        this.countOfChilden = countOfChilden;
    }


    public int getCountOfChilden() {
        return countOfChilden;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public City getCity() {
        return city;
    }

    public String toString() {
        return "Age =" + age +
                " Gender =" + gender +
                " First name =" + firstName +
                " Second name =" + secondName +
                " Count of children =" + countOfChilden +
                " City =" + city;
    }
}

