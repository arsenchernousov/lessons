package homework.inheritance;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ReaderFile extends Reader {//is a

    private Object object;//has a

    @Override
    public String read() throws IOException {
        Path path = Paths.get("./data/Poem.txt");

        BufferedReader reader = Files.newBufferedReader(path);

        String line = null;
        String result = "";

        StringBuilder builder = new StringBuilder();

        while ((line = reader.readLine()) != null) {//hello
//            result += line;// "" + hello
            builder.append(line);
        }


        return modifyText(builder.toString());
    }
}
