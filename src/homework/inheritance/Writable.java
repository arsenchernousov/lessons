package homework.inheritance;

import java.io.IOException;

public interface Writable {

    abstract void write(String text) throws IOException;
}
