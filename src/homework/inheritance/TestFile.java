package homework.inheritance;

import java.io.IOException;
import java.util.Scanner;

public class TestFile {
    public static void main(String[] args) throws IOException {

        System.out.println("Enter your text");
        Scanner scanner = new Scanner(System.in);
        String textOfUser = scanner.nextLine();

        new WriterFile().write(textOfUser);

        ReaderFile readerFile = new ReaderFile();

//        String read = readerFile.read();
        System.out.println(readerFile.read());
    }
}
