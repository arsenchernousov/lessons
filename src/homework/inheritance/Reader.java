package homework.inheritance;


public abstract class Reader implements Readable {
    public String modifyText(String text) {
        return text.replaceAll("I'm ready for writting to file", " I'm from file");
    }
}
