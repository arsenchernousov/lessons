package homework.inheritance;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class WriterFile extends Writer {

    public void write(String text) throws IOException {

        Path path = Paths.get("./data/Poem.txt");

        text = modifyText(text);

        Files.write(path, text.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    }


}
