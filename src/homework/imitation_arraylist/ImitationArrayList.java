package homework.imitation_arraylist;

public class ImitationArrayList {
    private int[] array;

    public ImitationArrayList() {
        array = new int[10];
    }

    public void addElement(int element) {
        reSize();
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                array[i] = element;
                break;
            }

        }
    }

    private void reSize() {
        boolean shouldResize = false;

        for (int element : array) {
            if (element == 0) {
                shouldResize = true;
                break;
            }

        }
        if (!shouldResize) {
            int[] temp = new int[array.length * 2];

            for (int i = 0; i < array.length; i++) {
                temp[i] = array[i];

            }
            array = temp;
        }
    }

    public void changeElementByIndex(int index, int element) {

        array[index] = element;
    }

    public void showElementInOrder() {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");

        }
    }

    public void showElementInReversOrder() {
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }
    }

    public void sortBubble() {
        for (int i = array.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
            System.out.print(array[i]+ " ");
        }
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }
}

